from tornado.web import url
from services import rpc
from web import admin, handlers

_routes = [
    # Main Routes
    (r'/', handlers.HomeHandler),
    (r'/rpc', rpc.ApiHandler)
]


def get_routes():
    return _routes
