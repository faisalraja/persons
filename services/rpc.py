from lib.basehandler import RpcHandler
from models import models


class ApiHandler(RpcHandler):

    async def save_person(self, person):
        p = await self.run_async(models.Person.save_person, **person)
        return p.to_json()

    async def list_person(self, page=1):
        persons = await self.run_async(models.Person.list_page, page)
        return [person.to_json() for person in persons]

    async def delete_person(self, person_id):
        if person_id:
            await self.run_async(models.Person.delete_by_id, person_id)
