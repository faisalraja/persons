import logging
import mimetypes
import os
import time
import uuid
from authomatic import Authomatic
from lib.auth.adapter import TornadoWebAdapter
from tornado import gen
import config
from lib.basehandler import BaseHandler
from models import models


class HomeHandler(BaseHandler):

    async def get(self):
        params = {
        }
        return self.render_template('main/index.html', **params)
