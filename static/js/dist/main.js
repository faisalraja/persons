'use strict';

Vue.component('person-edit', {
    data: function data() {
        return {
            loading: false,
            data: {},
            error: {}
        };
    },

    methods: {
        submit: function submit() {
            var self = this;
            self.error = {};
            if (!self.loading) {
                self.loading = true;
                self.rpc('save_person', {
                    person: self.data
                }).then(function (resp) {
                    if (resp.data.error) {
                        if (resp.data.error.code == -32001) {
                            self.error = resp.data.error.data;
                        }
                    } else if (resp.data.result && typeof self.onSuccess == 'function') {
                        self.onSuccess(resp.data.result);
                    }
                }).then(function () {
                    self.loading = false;
                });
            }
        }
    },
    props: ['initData', 'onSuccess'],
    created: function created() {
        if (this.initData) {
            this.data = this.initData;
        }
    },

    template: '\n        <form class="form-inline" v-on:submit.prevent="submit">\n            <div class="form-group" :class="{\'has-error\': error.first_name}">\n                <label class="sr-only" for="first_name">First Name</label>\n                <input type="text" class="form-control" id="first_name" v-model="data.first_name" placeholder="First Name">\n            </div>            \n            <div class="form-group" :class="{\'has-error\': error.last_name}">\n                <label class="sr-only" for="last_name">Last Name</label>\n                <input type="text" class="form-control" id="last_name" v-model="data.last_name" placeholder="Last Name">\n            </div>            \n            <div class="form-group" :class="{\'has-error\': error.birth_date}">\n                <label class="sr-only" for="birth_date">Birthday</label>\n                <input type="text" class="form-control" id="birth_date" v-model="data.birth_date" placeholder="Birth Date (YYYY-MM-DD)">\n            </div>            \n            <div class="form-group" :class="{\'has-error\': error.zip_code}">\n                <label class="sr-only" for="zip_code">Zip Code</label>\n                <input type="text" class="form-control" id="zip_code" v-model="data.zip_code" placeholder="Zip Code">\n            </div>\n            <button type="submit" class="btn btn-default" :disabled="loading">{{ loading ? \'Saving...\' : \'Save\' }}</button>\n        </form>\n    '
});

Vue.component('persons', {
    data: function data() {
        return {
            persons: [],
            page: 1,
            loading: false,
            hasMore: false,
            errors: null,
            edit: null,
            editIndex: null
        };
    },

    methods: {
        loadData: function loadData() {
            var self = this;
            if (!self.loading) {
                self.loading = true;
                self.rpc('list_person', {
                    page: self.page
                }).then(function (resp) {
                    if (resp.data.error) {
                        self.hasMore = false;
                        self.errors = resp.data.error.data || resp.data.error.message;
                    } else {
                        self.hasMore = resp.data.result.length >= 10;
                        self.persons = self.persons.concat(resp.data.result);
                    }
                }).then(function () {
                    self.loading = false;
                });
            }
            self.page++;
        },
        deletePerson: function deletePerson(index) {
            this.rpc('delete_person', {
                person_id: this.persons[index].id
            });
            this.persons.splice(index, 1);
        },
        onSave: function onSave(person) {
            if (this.editIndex !== null) {
                this.persons[this.editIndex] = person;
            } else {
                this.persons.push(person);
            }
            this.edit = null;
            this.editIndex = null;
        },
        editPerson: function editPerson(index) {
            this.editIndex = index;
            this.edit = JSON.parse(JSON.stringify(this.persons[index]));
        }
    },
    mounted: function mounted() {
        this.loadData();
    },

    template: '\n        <div>\n            <div v-show="loading">Loading...</div>\n            <div v-show="!loading">\n                <person-edit v-if="edit" :init-data="edit" :on-success="onSave"></person-edit>\n                <button type="button" class="btn btn-info" v-show="!edit" v-on:click="edit = {}">Add</button>\n                <div v-show="errors" v-text="errors" class="alert alert-danger"></div>\n                <table class="table">\n                    <thead>\n                    <tr>\n                        <th>First Name</th>\n                        <th>Last Name</th>\n                        <th>Birthdate</th>\n                        <th>Zip</th>\n                        <th></th>\n                    </tr>\n                    </thead>\n                    <tbody>\n                    <tr v-for="(person, index) in persons" :key="person.id">\n                        <td v-text="person.first_name"></td>\n                        <td v-text="person.last_name"></td>\n                        <td v-text="person.birth_date"></td>\n                        <td v-text="person.zip_code"></td>\n                        <td>\n                            <button type="button" class="btn btn-info" v-on:click="editPerson(index)">Edit</button>\n                            <button type="button" class="btn btn-info" v-on:click="deletePerson(index)">Delete</button>\n                        </td>\n                    </tr>\n                    </tbody>\n                </table>\n                <ul v-show="hasMore && !loading" class="pager">\n                    <li><a v-show="!loading && hasMore" v-on:click="loadData()">Load More</a></li>\n                </ul>\n            </div>\n        </div>\n    '
});

Vue.mixin({
    methods: {
        rpc: function rpc(method, params) {
            return axios({
                method: 'post',
                url: '/rpc',
                data: { method: method, id: 1, jsonrpc: '2.0', params: params }
            });
        }
    }
});

new Vue({
    el: '#app'
});