import datetime

import cerberus
from peewee import *
from playhouse.shortcuts import model_to_dict

from lib.basemodel import BaseModel
from lib.jsonrpc import ServerException


class Person(BaseModel):
    first_name = CharField()
    last_name = CharField()
    birth_date = DateField(formats='%Y-%m-%d')
    zip_code = CharField()

    @classmethod
    def save_person(cls, **kwargs):
        validator = cerberus.Validator({
            'first_name': {'required': True, 'empty': False},
            'last_name': {'required': True, 'empty': False},
            'birth_date': {'required': True, 'type': 'string',
                           'regex': '[\d]{4}-[\d]{2}-[\d]{2}'},
            'zip_code': {'required': True, 'minlength': 5, 'maxlength': 5}
        })

        person = cls()
        if 'id' in kwargs:
            person = cls.get_by_id(kwargs.pop('id'))
            if person is None:
                raise ServerException('Person not found')

        if not validator.validate(kwargs):
            raise ServerException(validator.errors, 'Validation Error', -32001)

        person.first_name = kwargs['first_name']
        person.last_name = kwargs['last_name']
        person.birth_date = kwargs['birth_date']
        person.zip_code = kwargs['zip_code']
        person.save()

        return person

    @classmethod
    def list_page(cls, page, limit=10):

        return cls.select().order_by(cls.first_name).paginate(page, limit)

    @classmethod
    def delete_by_id(cls, person_id):
        qry = cls.delete().where(cls.id == person_id)
        qry.execute()

    def to_json(self):
        data = model_to_dict(self)
        if type(self.birth_date) is datetime.date:
            data['birth_date'] = self.birth_date.strftime('%Y-%m-%d')
        return data