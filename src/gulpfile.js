const gulp =require('gulp');
const watch = require('gulp-watch');
const sass = require('gulp-sass');
const minifyjs = require('gulp-js-minify');
const rename = require("gulp-rename");
const runSequence = require('run-sequence');
const babel = require('gulp-babel');
const rollup = require('gulp-rollup');

const buildJs = function (env) {
    const content = gulp.src(['js/**/*.js'])
        .pipe(rollup({
            entry: [
                './js/main.js'
            ],
            format: 'es'
        }))
        .pipe(babel({
            "presets": [
                "es2015"
            ],
            "plugins": [
                "transform-async-to-module-method",
                "syntax-async-functions"
            ]
        }));

    if (env != 'dev') {
        content
            .pipe(minifyjs())
            .pipe(rename({ suffix: '.min' }))
    }

    return content.pipe(gulp.dest('../static/js/dist/'));
};

gulp.task('watch', function() {
    runSequence('build', () => {
        "use strict";

        gulp.watch('js/**/*.js', ['build-js']);
    });
});

gulp.task('build-js', function() {
    return buildJs('dev');
});

gulp.task('build', function(cb) {
    runSequence(
        'build-js',
        cb);
});