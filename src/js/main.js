"use strict";
import "./component/persons";

Vue.mixin({
    methods: {
        rpc(method, params) {
            return axios({
                method: 'post',
                url: '/rpc',
                data: {method: method, id: 1, jsonrpc: '2.0', params: params}
            });
        }
    }
});

new Vue({
    el: '#app'
});