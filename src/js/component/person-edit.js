"use strict";

Vue.component('person-edit', {
    data () {
        return {
            loading: false,
            data: {},
            error: {}
        };
    },
    methods: {
        submit() {
            let self = this;
            self.error = {};
            if (!self.loading) {
                self.loading = true;
                self.rpc('save_person', {
                    person: self.data
                }).then(function (resp) {
                    if (resp.data.error) {
                        if (resp.data.error.code == -32001) {
                            self.error = resp.data.error.data;
                        }
                    } else if (resp.data.result && typeof self.onSuccess == 'function') {
                        self.onSuccess(resp.data.result);
                    }
                }).then(function () {
                    self.loading = false;
                });
            }
        }
    },
    props: ['initData', 'onSuccess'],
    created() {
        if (this.initData) {
            this.data = this.initData;

        }
    },
    template: `
        <form class="form-inline" v-on:submit.prevent="submit">
            <div class="form-group" :class="{'has-error': error.first_name}">
                <label class="sr-only" for="first_name">First Name</label>
                <input type="text" class="form-control" id="first_name" v-model="data.first_name" placeholder="First Name">
            </div>            
            <div class="form-group" :class="{'has-error': error.last_name}">
                <label class="sr-only" for="last_name">Last Name</label>
                <input type="text" class="form-control" id="last_name" v-model="data.last_name" placeholder="Last Name">
            </div>            
            <div class="form-group" :class="{'has-error': error.birth_date}">
                <label class="sr-only" for="birth_date">Birthday</label>
                <input type="text" class="form-control" id="birth_date" v-model="data.birth_date" placeholder="Birth Date (YYYY-MM-DD)">
            </div>            
            <div class="form-group" :class="{'has-error': error.zip_code}">
                <label class="sr-only" for="zip_code">Zip Code</label>
                <input type="text" class="form-control" id="zip_code" v-model="data.zip_code" placeholder="Zip Code">
            </div>
            <button type="submit" class="btn btn-default" :disabled="loading">{{ loading ? 'Saving...' : 'Save' }}</button>
        </form>
    `
});