"use strict";
import "./person-edit";

Vue.component('persons', {
    data () {
        return {
            persons: [],
            page: 1,
            loading: false,
            hasMore: false,
            errors: null,
            edit: null,
            editIndex: null
        };
    },
    methods: {
        loadData() {
            let self = this;
            if (!self.loading) {
                self.loading = true;
                self.rpc('list_person', {
                    page: self.page
                }).then(function (resp) {
                    if (resp.data.error) {
                        self.hasMore = false;
                        self.errors = resp.data.error.data || resp.data.error.message;
                    } else {
                        self.hasMore = resp.data.result.length >= 10;
                        self.persons = self.persons.concat(resp.data.result);
                    }
                }).then(function () {
                    self.loading = false;
                });
            }
            self.page ++;
        },
        deletePerson(index) {
            this.rpc('delete_person', {
                person_id: this.persons[index].id
            });
            this.persons.splice(index, 1);
        },
        onSave(person) {
            if (this.editIndex !== null) {
                this.persons[this.editIndex] = person;
            } else {
                this.persons.push(person);
            }
            this.edit = null;
            this.editIndex = null;
        },
        editPerson(index) {
            this.editIndex = index;
            this.edit = JSON.parse(JSON.stringify(this.persons[index]));
        }
    },
    mounted() {
        this.loadData();
    },
    template: `
        <div>
            <div v-show="loading">Loading...</div>
            <div v-show="!loading">
                <person-edit v-if="edit" :init-data="edit" :on-success="onSave"></person-edit>
                <button type="button" class="btn btn-info" v-show="!edit" v-on:click="edit = {}">Add</button>
                <div v-show="errors" v-text="errors" class="alert alert-danger"></div>
                <table class="table">
                    <thead>
                    <tr>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Birthdate</th>
                        <th>Zip</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr v-for="(person, index) in persons" :key="person.id">
                        <td v-text="person.first_name"></td>
                        <td v-text="person.last_name"></td>
                        <td v-text="person.birth_date"></td>
                        <td v-text="person.zip_code"></td>
                        <td>
                            <button type="button" class="btn btn-info" v-on:click="editPerson(index)">Edit</button>
                            <button type="button" class="btn btn-info" v-on:click="deletePerson(index)">Delete</button>
                        </td>
                    </tr>
                    </tbody>
                </table>
                <ul v-show="hasMore && !loading" class="pager">
                    <li><a v-show="!loading && hasMore" v-on:click="loadData()">Load More</a></li>
                </ul>
            </div>
        </div>
    `
});